[
  (let
    src = builtins.fetchGit {
      url = "https://github.com/gittuf/gittuf.git";
      rev = "f93046179dba8dd55170b7f49e43b012bff58a6a";
    };
    vendorHash = "sha256-R4lqCCibSfmG6o9q9hPR/imafGytQanuad3dbuGxNFU=";
  in
    self: super: {
      gittuf = super.gittuf.override {
        buildGoModule = args: super.buildGoModule (args // {inherit src vendorHash;});
      };
    })
]
