{
  inputs.nixpkgs.url = "github:nixos/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = {
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = import ./overlays.nix;
        };
      in {
        packages = {
          fetchgittuf = pkgs.callPackage ./fetcher.nix {};
          inherit (pkgs) gittuf;
        };
      }
    );
}
