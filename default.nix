let
  pkgs = import (builtins.fetchGit {
    url = "https://github.com/nixos/nixpkgs.git";
    rev = "4a5f70933fde466bb3bfb37846ee0642488513c9";
  }) {overlays = import ./overlays.nix;};
in
  pkgs.callPackage ./fetcher.nix {}
