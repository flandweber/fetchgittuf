{
  stdenv,
  git,
  gittuf,
  writeTextDir,
}: ({
    name ? "source",
    url,
    ref,
    root-keys,
    rev ? null,
    outputHash,
  }:
    assert root-keys != [];
      stdenv.mkDerivation {
        inherit name url ref rev outputHash;

        buildInputs = [gittuf git];
        phases = ["installPhase"];
        installPhase = let
          git-config-dir =
            writeTextDir ".config/git/config"
            ''
              [safe]
                directory = *
            '';
        in ''
          HOME=${git-config-dir} gittuf --verbose clone "$url" src ${builtins.concatStringsSep " " (builtins.map (k: "--root-key  \"${k}\"") root-keys)}
          cd src
          gittuf --verbose verify-ref "$ref"
          if [ -n "$rev" ]
          then
            git merge-base --is-ancestor "$rev" "$ref" # check if $rev in $ref history
            git checkout --quiet "$rev"
          else
            git checkout --quiet "$ref"
          fi
          rm -r .git
          cd ..
          mv src $out
        '';

        outputHashAlgo = "sha256";
        outputHashMode = "recursive";
      })
